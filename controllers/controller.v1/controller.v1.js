export let renderProductListV1 = (list) => {
  let contentHTML = "";
  list.forEach((items) => {
    let content = `
        <div class="col-3 my-2">
            <div class="card ">
                <div class="card-header bg-white">
                <img src="${items.img}" class="card-img-top" alt="...">
                </div>
                <div class="card-body">
                    <h5 class="card-title">${items.name}</h5>
                    <p class="card-text">${items.desc}, Screen: ${items.screen}, Back camera: ${items.backCamera}, Font camera: ${items.frontCamera}</p>
                    <div class="d-flex justify-content-between">
                    <p class="">$ ${items.price}</p>
                    <button onclick="addToCart(${items.id})" type="button" name="" id="" class="btn gradient text-white" btn-lg btn-block">Add</button>
                    </div>
                </div>
            </div>
        </div>
        `;
    contentHTML += content;
  });
  document.getElementById("bodyShoppingCart").innerHTML = contentHTML;
};

export let renderCart = (list) => {
  console.log('list: ', list);

  let contentHTML = "";
  list.forEach((items, index) => {
    let content = `
        <tr>
            <td><img src="${items.product.img}" width="50px" alt="..."></td>
            <td>${items.product.name}</td>
            <td>$ ${((items.quantity) * (items.product.price))}</td>
            <td>
            <button onclick="reduceQty(${items.product.id}, 2)" class="btn btn-link px-2 text-white"> <i class="fas fa-minus"></i></button>
            <input name="" id="" class="btn" type="input" value="${items.quantity}"  style="width: 44px">
            <button onclick="increaseQty(${items.product.id}, 10)" class="btn btn-link px-2 text-white"> <i class="fas fa-plus"></i></button>
            </td>
            <td>
            <button onclick="removeItem(${index})" type="button" name="" id="" class="btn btn-lg btn-block">
            <i class="fa-solid fa-trash text-danger"></i>
            </button>

            </td>
            
        </tr>
        `;
    contentHTML += content;
  });
  document.getElementById("tbodyCart").innerHTML = contentHTML;
};




export let timViTriIndex = (id, arr) => {
  

  for (let index =0; index < arr.length; index++) {
      let itemCheck = arr[index].id;
   
  
      if (id == itemCheck) {
       
        return index;
      }
  }

};

window.timViTriIndex = timViTriIndex;

export let kiemTraGioHang = (idItem, arr) => {
    for (let index = 0; index < arr.length; index++) {
    let item = arr[index];
    let itemCheck = item.product.id;
    if (idItem == itemCheck) {
      
      return idItem;  
    }
  }
  
};
window.kiemTraGioHang = kiemTraGioHang;

export let timViTriIndexGioHang = (idItem, arr) => {
  for (let index = 0; index < arr.length; index++) {
  let item = arr[index];
  let itemCheck = item.product.id;
  if (idItem == itemCheck) {
    return index;  
  }
}

};
window.timViTriIndexGioHang = timViTriIndexGioHang;


export let tinhTongGioHang = (cart) => {
  
 let tongTien =0;
  for (let value of cart) {
    tongTien += value.totalPrice();
  }
  
  document.getElementById("txt-tongTien").innerHTML = `Tổng tiền: $ ${tongTien}`;

};

window.tinhTongGioHang = tinhTongGioHang;


export let showSLGioHang = (arr) => {
    let total =0;

    for (let value of arr) {
      total += value.quantity;
    }

  document.getElementById("quanlityCart").innerText = total;

};
window.showSLGioHang = showSLGioHang;


export let batLoading = () => {
  document.getElementById("loading").style.display = "flex";
};
window.batLoading = batLoading;
export let tatLoading =  () => {
  document.getElementById("loading").style.display = "none";
};
window.tatLoading = tatLoading;