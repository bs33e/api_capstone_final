import { CartItem } from "../../models/model.v1/model.v1.js";
import {
  renderProductListV1,
  renderCart,
  timViTriIndex,
  kiemTraGioHang,
  timViTriIndexGioHang,
  tinhTongGioHang,
  showSLGioHang,
  batLoading,
  tatLoading,
} from "./controller.v1.js";

let productList;

//let cart = [];

let cartJson = [];
console.log("cartJson: ", cartJson);
let DSSP = "DSSP";

const BASE_URL = "https://62f8b754e0564480352bf3c3.mockapi.io";

let renderShoppingCart = () => {
  batLoading();
  axios({
    url: `${BASE_URL}/phone`,
    method: "GET",
  })
    .then((res) => {
      console.log("res: ", res);
      tatLoading();
      productList = res.data;

      renderProductListV1(productList);
    })
    .catch((err) => {
      tatLoading();

      console.log("err: ", err);
    });
};
renderShoppingCart();

let dsspLocalStorage = localStorage.getItem(DSSP);
console.log("dsspLocalStorage: ", JSON.parse(dsspLocalStorage));

if (JSON.parse(dsspLocalStorage)) {
  let data = JSON.parse(dsspLocalStorage);
  for (let index = 0; index < data.length; index++) {
    let current = data[index];
    let products = new CartItem(
      current.product,
      current.quantity,
      current.totalPrice
    );
    cartJson.push(products);
  }
  renderCart(cartJson);
}

let saveLocalStorage = () => {
  let dsspJson = JSON.stringify(cartJson);
  localStorage.setItem(DSSP, dsspJson);
};

document.getElementById("quanlityCart").innerText = "";

showSLGioHang(cartJson);

let addToCart = (id) => {
  let item = timViTriIndex(id, productList);

  let quantity = 1;
  let data = productList[item];
  let cartItem = new CartItem(data, quantity);

  let itemCheck = kiemTraGioHang(id, cartJson);

  if (id == itemCheck) {
    let indexCart = timViTriIndexGioHang(id, cartJson);
    cartJson[indexCart].quantity++;
    saveLocalStorage();
    showSLGioHang(cartJson);
    return;
  } else {
    cartJson.push(cartItem);
    saveLocalStorage();
    showSLGioHang(cartJson);
  }
};

window.addToCart = addToCart;

let reduceQty = (id, min) => {
  let indexCart = timViTriIndexGioHang(id, cartJson);

  if (cartJson[indexCart].quantity < min) {
    saveLocalStorage();
    tinhTongGioHang(cartJson);
    renderCart(cartJson);
    showSLGioHang(cartJson);

    return;
  } else {
    cartJson[indexCart].quantity--;
    saveLocalStorage();
    tinhTongGioHang(cartJson);
    renderCart(cartJson);
    showSLGioHang(cartJson);
  }
};

window.reduceQty = reduceQty;

let increaseQty = (id, max) => {
  let indexCart = timViTriIndexGioHang(id, cartJson);

  if (cartJson[indexCart].quantity >= max) {
    saveLocalStorage();
    tinhTongGioHang(cartJson);
    renderCart(cartJson);
    showSLGioHang(cartJson);
    return;
  } else {
    cartJson[indexCart].quantity++;
    saveLocalStorage();
    tinhTongGioHang(cartJson);
    renderCart(cartJson);
    showSLGioHang(cartJson);
  }
};

window.increaseQty = increaseQty;

let showCart = () => {
  tinhTongGioHang(cartJson);
  renderCart(cartJson);
  showSLGioHang(cartJson);
};

window.showCart = showCart;

let filterItem = (brand) => {
  if (brand == "Apple") {
    let product = productList.filter((sanPham) => sanPham.type === "Apple");
    renderProductListV1(product);
  } else if (brand == "Samsung") {
    let samsung = productList.filter((sanPham) => sanPham.type === "Samsung");
    renderProductListV1(samsung);
  } else {
    renderProductListV1(productList);
  }
};

window.filterItem = filterItem;

let clearCart = () => {
  if (cartJson.length < 1) {
    return;
  }
  for (let index in cartJson) {
    console.log("index: ", index);
    cartJson.splice(index);
    saveLocalStorage();
    tinhTongGioHang(cartJson);
    showSLGioHang(cartJson);
    renderCart(cartJson);
  }
  document.getElementById(
    "tbodyCart"
  ).innerHTML = `<h5  class="text-white">Giỏ hàng trống.</h5>`;
};

window.clearCart = clearCart;

let payCart = () => {
  if (cartJson.length < 1) {
    return;
  }

  clearCart();
  document.getElementById(
    "tbodyCart"
  ).innerHTML = `<h5  class="text-white">Đã thanh toán thành công.</h5>`;
};

window.payCart = payCart;

let removeItem = (index) => {
  cartJson.splice(index, 1);
  saveLocalStorage();
  tinhTongGioHang(cartJson);
  showSLGioHang(cartJson);
  renderCart(cartJson);

  if (cartJson.length < 1) {
    document.getElementById(
      "tbodyCart"
    ).innerHTML = `<h5  class="text-white">Giỏ hàng trống.</h5>`;
  }
};
window.removeItem = removeItem;
