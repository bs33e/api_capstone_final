import {
  renderProductListV2,
  layThongTinTuFormList,
  showThongTinLenFormList,
  batLoading,
  tatLoading,

} from "./controller.v2.js";
import { ProductListV2 } from "../../models/productList.v2/productClass.v2.js";
import { validation } from "./validation.v2.js";

const BASE_URL = "https://62f8b754e0564480352bf3c3.mockapi.io";

let renderTableProduct = () => {
  batLoading();
  axios({
    url: `${BASE_URL}/phone`,
    method: "GET",
  })
    .then((res) => {
      tatLoading();
      console.log("res: ", res);
      renderProductListV2(res.data);
    })
    .catch((err) => {
      tatLoading();
      console.log("err: ", err);
    });
};

renderTableProduct();

document.getElementById("themMoiSP").addEventListener("click", () => {
  document.getElementById("btnThemSP").classList.remove("d-none");
  document.getElementById("btnCapNhatSP").classList.add("d-none");
});

let addProduct = () => {
  
  let { id, name, price, screen, backCamera, frontCamera, img, desc, type } =
    layThongTinTuFormList();
  let productInput = new ProductListV2(
    id,
    name,
    price,
    screen,
    backCamera,
    frontCamera,
    img,
    desc,
    type
  );

  // Kiểm tra name
  let isValid = validation.kiemTraRong(productInput.name, "tbName") && validation.kiemTraDoDaiKyTu(productInput.name, "tbName", 6, 40);
  // Kiểm tra price
  isValid = isValid & validation.kiemTraRong(productInput.price, "tbPrice")  && validation.kiemTraSo(productInput.price, "tbPrice") ;
  // Kiểm tra screen
  isValid = isValid & validation.kiemTraRong(productInput.screen, "tbScreen") && validation.kiemTraKichThuocManHinh(productInput.screen, "tbScreen", 65);
 // Kiểm tra back camera
  isValid = isValid & validation.kiemTraRong(productInput.backCamera, "tbBackCam") && validation.kiemTraDoDaiKyTu(productInput.backCamera, "tbBackCam", 2, 40);
  // Kiểm tra font camera
  isValid = isValid & validation.kiemTraRong(productInput.frontCamera, "tbFontCam") && validation.kiemTraDoDaiKyTu(productInput.frontCamera, "tbFontCam", 2, 40);
  // Kiểm tra link hinh anh
  isValid = isValid & validation.kiemTraRong(productInput.img, "tbHinhAnh");
  // Kiểm tra mo ta
  isValid = isValid & validation.kiemTraRong(productInput.desc, "tbMoTa") && validation.kiemTraDoDaiKyTu(productInput.desc, "tbMoTa", 10, 140);
  if (isValid == false) {
    return;
  }

  batLoading();
  axios({
    url: `${BASE_URL}/phone`,
    method: "POST",
    data: productInput,
  })
    .then((res) => {
      tatLoading();
      console.log("res: ", res);
      renderTableProduct();
     
      
    })
    .catch((err) => {
      tatLoading();
      console.log("err: ", err);
    });
};
window.addProduct = addProduct;

let deleteProduct = (id) => {
 
  batLoading();
  axios({
    url: `${BASE_URL}/phone/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      tatLoading();
      console.log("res: ", res);
      renderTableProduct();
    })
    .catch((err) => {
      tatLoading();
      console.log("err: ", err);
    });
};

window.deleteProduct = deleteProduct;


let editProduct = (id) => {
  document.getElementById("btnThemSP").classList.add("d-none");
  document.getElementById("btnCapNhatSP").classList.remove("d-none");
  batLoading();
  axios({
    url: `${BASE_URL}/phone/${id}`,
    method: "GET",
  })
    .then((res) => {
      tatLoading();
      console.log("res: ", res);
      showThongTinLenFormList(res.data);
    })
    .catch((err) => {
      tatLoading();
      console.log("err: ", err);
    });
};

window.editProduct = editProduct;

let updateProduct = () => {
  let dataUpdate = layThongTinTuFormList();
  console.log("dataUpdate.id: ", dataUpdate.id);
  batLoading();
  axios({
    url: `${BASE_URL}/phone/${dataUpdate.id}`,
    method: "PUT",
    data: dataUpdate,
  })
    .then((res) => {
      tatLoading();
      console.log("res: ", res);
      renderTableProduct();
    })
    .catch((err) => {
      tatLoading();
      console.log("err: ", err);
    });
};
window.updateProduct = updateProduct;

